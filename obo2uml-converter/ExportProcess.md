Exporting procedure
===================

1. Right click in the project and select `Export...`
2. Under Java, select "Runnable Jar file" and proceed.
3. Select the following options:
    Launch Configuration: "OBO@UML Jar Launch Config - obo2uml"
    Exported destination: /the/desired/folder/obo2uml.jar
    Library handling: "Package required libraries into generated JAR"
4. Click Finish.
