package br.com.cawal.obo2umlconverter;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Argument parser to OBO2UML program.
 * Returns a populated ExecutionConfig instance
 * @author cawal
 *
 */
public class ArgumentParser {
	

	public static ExecutionConfig parse(String[] args){
		ExecutionConfig execConfig = new ExecutionConfig();
		execConfig.setSourceMetamodel("OBOFFF"); // default
		execConfig.setTargetMetamodel("UML"); // default
		
		CommandLineParser parser = new DefaultParser();
		Options options = getOptions();

		try {
			CommandLine cmd = parser.parse(options, args);
			
			if(cmd.hasOption("o")){
				List<String> outputPaths = Arrays.asList(cmd.getOptionValues("o"));
				
				for(String path : outputPaths){
					File outputFile = new File(path);
					execConfig.getOutputFiles().add(outputFile);
				}
			}
			
			if(cmd.hasOption("i")){
				List<String> inputPaths = Arrays.asList(cmd.getOptionValues("i"));
				
				for(String path : inputPaths){
					File inputFile = new File(path);
					execConfig.getInputFiles().add(inputFile);
				}
			}

			if(cmd.hasOption("k")){
			  execConfig.setKeepIntermediaryFiles(true);
            }
			
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -jar obo2uml.jar", options );
			System.exit(1);
		}
		
	    if(execConfig.getInputFiles().size() == 0 || execConfig.getOutputFiles().size()==0) {
          HelpFormatter formatter = new HelpFormatter();
          formatter.printHelp( "java -jar obo2uml.jar "
              + "[options] "
              + "-i <inputObo1>[:inputObo2...] -o <outputUml>", options );
          System.exit(1);
	    }

		
		return execConfig;
	}
	
	
	public static Options getOptions(){

		Options options = new Options(); 
		
		Option outputFile = Option.builder("o")
				.longOpt("output-file")
				.desc("Output file for writing the final model.")
				.hasArgs()
				.argName("output file")
				.build();
		options.addOption(outputFile);
		
		Option inputFiles = Option.builder("i")
				.longOpt("input-files")
				.desc("Input OBO files containing the ontology.")
				.hasArgs()
				.valueSeparator(':')
				.argName("input obo files")
				.build();
		options.addOption(inputFiles);
		
	   Option maintainFiles = Option.builder("k")
              .longOpt("keep-files")
              .desc("Don't remove intermediary files after transformation.")
              .argName("keep intermediary files")
              .build();
        options.addOption(maintainFiles);
      
		return options;
	}
	
}
