package br.com.cawal.obo2umlconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bbop.dataadapter.DataAdapterException;
import org.obo.dataadapter.OBOParseException;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBOAPI2OBODatamodelTranslator;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBODatamodelSerializer;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OboFileSerializer;


public class OBO2UML {

  public static void main(String[] args)
      throws MalformedURLException, IOException, OBOParseException, DataAdapterException {
    // Parse command line arguments
    ExecutionConfig executionConfig = ArgumentParser.parse(args);

    // get input paths
    List<String> inputPaths = new ArrayList<String>();
    executionConfig.getInputFiles().stream().map(f -> f.getAbsolutePath()).forEach(inputPaths::add);

    // Create the names of the intermediary files.
    String intermediaryFile = UUID.randomUUID() + ".obodatamodel";

    // Get the final file
    File outputUML = executionConfig.getOutputFiles().get(0);

    // INJECTION TO ODM
    System.out.println("Loading...");
    org.obo.datamodel.OBOSession session = OboFileSerializer.desserializeFromPaths(inputPaths);
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(session);
    OBOSession odmSession = translator.translate();

    // saving intermediary file
    OBODatamodelSerializer.serialize(odmSession, intermediaryFile);

    // transformation to UML and serialization
    InputStream intermediaryFileIS = new FileInputStream(new File(intermediaryFile));
    OutputStream outputUMLOS = new FileOutputStream(outputUML);
    TransformationRunner.transformOboRoOdm2UmlFile(intermediaryFileIS, outputUMLOS, null);

    // remove intermediary files
    if (!executionConfig.keepIntermediaryFiles()) {
      File f = new File(intermediaryFile);
      f.delete();
    }
    System.out.println("OBO2UML transformation complete.");

  }

}
