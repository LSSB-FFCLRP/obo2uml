<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="OBODatamodel2UMLBFOSuperimposition"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="entityKindString"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="bfo-entity-kind"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="10:41-10:58"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchOBOClass():V"/>
		<constant value="A.__matchOBOClass_BFOMinimun():V"/>
		<constant value="__exec__"/>
		<constant value="OBOClass"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyOBOClass(NTransientLink;):V"/>
		<constant value="OBOClass_BFOMinimun"/>
		<constant value="A.__applyOBOClass_BFOMinimun(NTransientLink;):V"/>
		<constant value="isBFOClass"/>
		<constant value="MOBO!OBOClass;"/>
		<constant value="0"/>
		<constant value="propertyValues"/>
		<constant value="property"/>
		<constant value="J.=(J):J"/>
		<constant value="B.or(B):B"/>
		<constant value="13:5-13:9"/>
		<constant value="13:5-13:24"/>
		<constant value="13:38-13:40"/>
		<constant value="13:38-13:49"/>
		<constant value="13:52-13:62"/>
		<constant value="13:52-13:79"/>
		<constant value="13:38-13:79"/>
		<constant value="13:5-13:80"/>
		<constant value="pv"/>
		<constant value="getBFOEntityKind"/>
		<constant value="B.not():B"/>
		<constant value="16"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.first():J"/>
		<constant value="16:5-16:9"/>
		<constant value="16:5-16:24"/>
		<constant value="16:38-16:40"/>
		<constant value="16:38-16:49"/>
		<constant value="16:52-16:62"/>
		<constant value="16:52-16:79"/>
		<constant value="16:38-16:79"/>
		<constant value="16:5-16:80"/>
		<constant value="16:5-17:15"/>
		<constant value="16:5-18:12"/>
		<constant value="__matchOBOClass"/>
		<constant value="OBO"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="J.isBFOClass():J"/>
		<constant value="J.not():J"/>
		<constant value="41"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="input"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="stereotype"/>
		<constant value="stereotypes"/>
		<constant value="J.get(J):J"/>
		<constant value="NTransientLink;.addVariable(SJ):V"/>
		<constant value="output"/>
		<constant value="Class"/>
		<constant value="UML"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="24:28-24:33"/>
		<constant value="24:28-24:46"/>
		<constant value="24:24-24:46"/>
		<constant value="26:32-26:42"/>
		<constant value="26:32-26:54"/>
		<constant value="26:59-26:69"/>
		<constant value="26:32-26:70"/>
		<constant value="29:3-32:4"/>
		<constant value="__applyOBOClass"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="NTransientLink;.getVariable(S):J"/>
		<constant value="4"/>
		<constant value="parents"/>
		<constant value="5"/>
		<constant value="sourceProperty"/>
		<constant value="J.resolveTemp(JJ):J"/>
		<constant value="ownedAttribute"/>
		<constant value="J.applyStereotype(J):J"/>
		<constant value="id"/>
		<constant value="J.setValue(JJJ):J"/>
		<constant value="definition"/>
		<constant value="30:12-30:17"/>
		<constant value="30:12-30:22"/>
		<constant value="30:4-30:22"/>
		<constant value="31:22-31:27"/>
		<constant value="31:22-31:35"/>
		<constant value="31:49-31:59"/>
		<constant value="31:72-31:73"/>
		<constant value="31:74-31:90"/>
		<constant value="31:49-31:91"/>
		<constant value="31:22-31:92"/>
		<constant value="31:4-31:92"/>
		<constant value="34:3-34:9"/>
		<constant value="34:26-34:36"/>
		<constant value="34:3-34:38"/>
		<constant value="35:3-35:9"/>
		<constant value="35:19-35:29"/>
		<constant value="35:31-35:35"/>
		<constant value="35:37-35:42"/>
		<constant value="35:37-35:45"/>
		<constant value="35:3-35:47"/>
		<constant value="36:3-36:9"/>
		<constant value="36:19-36:29"/>
		<constant value="36:31-36:43"/>
		<constant value="36:45-36:50"/>
		<constant value="36:45-36:61"/>
		<constant value="36:3-36:63"/>
		<constant value="33:2-37:3"/>
		<constant value="p"/>
		<constant value="link"/>
		<constant value="__matchOBOClass_BFOMinimun"/>
		<constant value="85"/>
		<constant value="bfoStereotype"/>
		<constant value="J.getBFOEntityKind():J"/>
		<constant value="model"/>
		<constant value="Model"/>
		<constant value="OUT"/>
		<constant value="J.allInstancesFrom(J):J"/>
		<constant value="bfoProfile"/>
		<constant value="Profile"/>
		<constant value="J.allInstances():J"/>
		<constant value="BFO"/>
		<constant value="72"/>
		<constant value="45:24-45:29"/>
		<constant value="45:24-45:42"/>
		<constant value="47:32-47:42"/>
		<constant value="47:32-47:54"/>
		<constant value="47:59-47:69"/>
		<constant value="47:32-47:70"/>
		<constant value="48:35-48:45"/>
		<constant value="48:35-48:57"/>
		<constant value="48:62-48:67"/>
		<constant value="48:62-48:86"/>
		<constant value="48:35-48:87"/>
		<constant value="50:23-50:32"/>
		<constant value="50:50-50:55"/>
		<constant value="50:23-50:56"/>
		<constant value="50:23-50:65"/>
		<constant value="51:31-51:42"/>
		<constant value="51:31-51:57"/>
		<constant value="51:72-51:73"/>
		<constant value="51:72-52:10"/>
		<constant value="52:13-52:18"/>
		<constant value="51:72-52:18"/>
		<constant value="51:31-52:19"/>
		<constant value="51:31-52:30"/>
		<constant value="55:3-58:4"/>
		<constant value="__applyOBOClass_BFOMinimun"/>
		<constant value="6"/>
		<constant value="7"/>
		<constant value="8"/>
		<constant value="J.applyProfile(J):J"/>
		<constant value="56:12-56:17"/>
		<constant value="56:12-56:22"/>
		<constant value="56:4-56:22"/>
		<constant value="57:22-57:27"/>
		<constant value="57:22-57:35"/>
		<constant value="57:49-57:59"/>
		<constant value="57:72-57:73"/>
		<constant value="57:74-57:90"/>
		<constant value="57:49-57:91"/>
		<constant value="57:22-57:92"/>
		<constant value="57:4-57:92"/>
		<constant value="62:3-62:8"/>
		<constant value="62:22-62:32"/>
		<constant value="62:3-62:34"/>
		<constant value="65:3-65:9"/>
		<constant value="65:26-65:36"/>
		<constant value="65:3-65:38"/>
		<constant value="66:3-66:9"/>
		<constant value="66:19-66:29"/>
		<constant value="66:31-66:35"/>
		<constant value="66:37-66:42"/>
		<constant value="66:37-66:45"/>
		<constant value="66:3-66:47"/>
		<constant value="67:3-67:9"/>
		<constant value="67:19-67:29"/>
		<constant value="67:31-67:43"/>
		<constant value="67:45-67:50"/>
		<constant value="67:45-67:61"/>
		<constant value="67:3-67:63"/>
		<constant value="70:3-70:9"/>
		<constant value="70:26-70:39"/>
		<constant value="70:3-70:41"/>
		<constant value="59:2-71:3"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<field name="5" type="4"/>
	<operation name="6">
		<context type="7"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="8"/>
			<push arg="9"/>
			<new/>
			<dup/>
			<push arg="10"/>
			<pcall arg="11"/>
			<dup/>
			<push arg="12"/>
			<push arg="9"/>
			<new/>
			<dup/>
			<push arg="13"/>
			<pcall arg="11"/>
			<pcall arg="14"/>
			<set arg="3"/>
			<getasm/>
			<push arg="15"/>
			<set arg="5"/>
			<getasm/>
			<push arg="16"/>
			<push arg="9"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="17"/>
			<getasm/>
			<pcall arg="18"/>
		</code>
		<linenumbertable>
			<lne id="19" begin="17" end="17"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="20" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="21">
		<context type="7"/>
		<parameters>
			<parameter name="22" type="4"/>
		</parameters>
		<code>
			<load arg="22"/>
			<getasm/>
			<get arg="3"/>
			<call arg="23"/>
			<if arg="24"/>
			<getasm/>
			<get arg="1"/>
			<load arg="22"/>
			<call arg="25"/>
			<dup/>
			<call arg="26"/>
			<if arg="27"/>
			<load arg="22"/>
			<call arg="28"/>
			<goto arg="29"/>
			<pop/>
			<load arg="22"/>
			<goto arg="30"/>
			<push arg="31"/>
			<push arg="9"/>
			<new/>
			<load arg="22"/>
			<iterate/>
			<store arg="32"/>
			<getasm/>
			<load arg="32"/>
			<call arg="33"/>
			<call arg="34"/>
			<enditerate/>
			<call arg="35"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="36" begin="23" end="27"/>
			<lve slot="0" name="20" begin="0" end="29"/>
			<lve slot="1" name="37" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="38">
		<context type="7"/>
		<parameters>
			<parameter name="22" type="4"/>
			<parameter name="32" type="39"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="22"/>
			<call arg="25"/>
			<load arg="22"/>
			<load arg="32"/>
			<call arg="40"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="20" begin="0" end="6"/>
			<lve slot="1" name="37" begin="0" end="6"/>
			<lve slot="2" name="41" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="42">
		<context type="7"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="43"/>
			<getasm/>
			<pcall arg="44"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="20" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="45">
		<context type="7"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="46"/>
			<call arg="47"/>
			<iterate/>
			<store arg="22"/>
			<getasm/>
			<load arg="22"/>
			<pcall arg="48"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="49"/>
			<call arg="47"/>
			<iterate/>
			<store arg="22"/>
			<getasm/>
			<load arg="22"/>
			<pcall arg="50"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="36" begin="5" end="8"/>
			<lve slot="1" name="36" begin="15" end="18"/>
			<lve slot="0" name="20" begin="0" end="19"/>
		</localvariabletable>
	</operation>
	<operation name="51">
		<context type="52"/>
		<parameters>
		</parameters>
		<code>
			<pushf/>
			<load arg="53"/>
			<get arg="54"/>
			<iterate/>
			<store arg="22"/>
			<load arg="22"/>
			<get arg="55"/>
			<getasm/>
			<get arg="5"/>
			<call arg="56"/>
			<call arg="57"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="58" begin="1" end="1"/>
			<lne id="59" begin="1" end="2"/>
			<lne id="60" begin="5" end="5"/>
			<lne id="61" begin="5" end="6"/>
			<lne id="62" begin="7" end="7"/>
			<lne id="63" begin="7" end="8"/>
			<lne id="64" begin="5" end="9"/>
			<lne id="65" begin="0" end="11"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="66" begin="4" end="10"/>
			<lve slot="0" name="20" begin="0" end="11"/>
		</localvariabletable>
	</operation>
	<operation name="67">
		<context type="52"/>
		<parameters>
		</parameters>
		<code>
			<push arg="31"/>
			<push arg="9"/>
			<new/>
			<load arg="53"/>
			<get arg="54"/>
			<iterate/>
			<store arg="22"/>
			<load arg="22"/>
			<get arg="55"/>
			<getasm/>
			<get arg="5"/>
			<call arg="56"/>
			<call arg="68"/>
			<if arg="69"/>
			<load arg="22"/>
			<call arg="70"/>
			<enditerate/>
			<call arg="71"/>
			<get arg="37"/>
		</code>
		<linenumbertable>
			<lne id="72" begin="3" end="3"/>
			<lne id="73" begin="3" end="4"/>
			<lne id="74" begin="7" end="7"/>
			<lne id="75" begin="7" end="8"/>
			<lne id="76" begin="9" end="9"/>
			<lne id="77" begin="9" end="10"/>
			<lne id="78" begin="7" end="11"/>
			<lne id="79" begin="0" end="16"/>
			<lne id="80" begin="0" end="17"/>
			<lne id="81" begin="0" end="18"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="66" begin="6" end="15"/>
			<lve slot="0" name="20" begin="0" end="18"/>
		</localvariabletable>
	</operation>
	<operation name="82">
		<context type="7"/>
		<parameters>
		</parameters>
		<code>
			<push arg="46"/>
			<push arg="83"/>
			<findme/>
			<push arg="84"/>
			<call arg="85"/>
			<iterate/>
			<store arg="22"/>
			<load arg="22"/>
			<call arg="86"/>
			<call arg="87"/>
			<call arg="68"/>
			<if arg="88"/>
			<getasm/>
			<get arg="1"/>
			<push arg="89"/>
			<push arg="9"/>
			<new/>
			<dup/>
			<push arg="46"/>
			<pcall arg="90"/>
			<dup/>
			<push arg="91"/>
			<load arg="22"/>
			<pcall arg="92"/>
			<dup/>
			<push arg="93"/>
			<getasm/>
			<get arg="94"/>
			<push arg="46"/>
			<call arg="95"/>
			<dup/>
			<store arg="32"/>
			<pcall arg="96"/>
			<dup/>
			<push arg="97"/>
			<push arg="98"/>
			<push arg="99"/>
			<new/>
			<pcall arg="100"/>
			<pusht/>
			<pcall arg="101"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="102" begin="7" end="7"/>
			<lne id="103" begin="7" end="8"/>
			<lne id="104" begin="7" end="9"/>
			<lne id="105" begin="26" end="26"/>
			<lne id="106" begin="26" end="27"/>
			<lne id="107" begin="28" end="28"/>
			<lne id="108" begin="26" end="29"/>
			<lne id="109" begin="33" end="38"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="93" begin="31" end="38"/>
			<lve slot="1" name="91" begin="6" end="40"/>
			<lve slot="0" name="20" begin="0" end="41"/>
		</localvariabletable>
	</operation>
	<operation name="110">
		<context type="7"/>
		<parameters>
			<parameter name="22" type="111"/>
		</parameters>
		<code>
			<load arg="22"/>
			<push arg="91"/>
			<call arg="112"/>
			<store arg="32"/>
			<load arg="22"/>
			<push arg="97"/>
			<call arg="113"/>
			<store arg="114"/>
			<load arg="22"/>
			<push arg="93"/>
			<call arg="115"/>
			<store arg="116"/>
			<load arg="114"/>
			<dup/>
			<getasm/>
			<load arg="32"/>
			<get arg="41"/>
			<call arg="33"/>
			<set arg="41"/>
			<dup/>
			<getasm/>
			<push arg="31"/>
			<push arg="9"/>
			<new/>
			<load arg="32"/>
			<get arg="117"/>
			<iterate/>
			<store arg="118"/>
			<getasm/>
			<load arg="118"/>
			<push arg="119"/>
			<call arg="120"/>
			<call arg="70"/>
			<enditerate/>
			<call arg="33"/>
			<set arg="121"/>
			<pop/>
			<load arg="114"/>
			<load arg="116"/>
			<pcall arg="122"/>
			<load arg="114"/>
			<load arg="116"/>
			<push arg="123"/>
			<load arg="32"/>
			<get arg="123"/>
			<pcall arg="124"/>
			<load arg="114"/>
			<load arg="116"/>
			<push arg="125"/>
			<load arg="32"/>
			<get arg="125"/>
			<pcall arg="124"/>
		</code>
		<linenumbertable>
			<lne id="126" begin="15" end="15"/>
			<lne id="127" begin="15" end="16"/>
			<lne id="128" begin="13" end="18"/>
			<lne id="129" begin="24" end="24"/>
			<lne id="130" begin="24" end="25"/>
			<lne id="131" begin="28" end="28"/>
			<lne id="132" begin="29" end="29"/>
			<lne id="133" begin="30" end="30"/>
			<lne id="134" begin="28" end="31"/>
			<lne id="135" begin="21" end="33"/>
			<lne id="136" begin="19" end="35"/>
			<lne id="109" begin="12" end="36"/>
			<lne id="137" begin="37" end="37"/>
			<lne id="138" begin="38" end="38"/>
			<lne id="139" begin="37" end="39"/>
			<lne id="140" begin="40" end="40"/>
			<lne id="141" begin="41" end="41"/>
			<lne id="142" begin="42" end="42"/>
			<lne id="143" begin="43" end="43"/>
			<lne id="144" begin="43" end="44"/>
			<lne id="145" begin="40" end="45"/>
			<lne id="146" begin="46" end="46"/>
			<lne id="147" begin="47" end="47"/>
			<lne id="148" begin="48" end="48"/>
			<lne id="149" begin="49" end="49"/>
			<lne id="150" begin="49" end="50"/>
			<lne id="151" begin="46" end="51"/>
			<lne id="152" begin="37" end="51"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="153" begin="27" end="32"/>
			<lve slot="4" name="93" begin="11" end="51"/>
			<lve slot="3" name="97" begin="7" end="51"/>
			<lve slot="2" name="91" begin="3" end="51"/>
			<lve slot="0" name="20" begin="0" end="51"/>
			<lve slot="1" name="154" begin="0" end="51"/>
		</localvariabletable>
	</operation>
	<operation name="155">
		<context type="7"/>
		<parameters>
		</parameters>
		<code>
			<push arg="46"/>
			<push arg="83"/>
			<findme/>
			<push arg="84"/>
			<call arg="85"/>
			<iterate/>
			<store arg="22"/>
			<load arg="22"/>
			<call arg="86"/>
			<call arg="68"/>
			<if arg="156"/>
			<getasm/>
			<get arg="1"/>
			<push arg="89"/>
			<push arg="9"/>
			<new/>
			<dup/>
			<push arg="49"/>
			<pcall arg="90"/>
			<dup/>
			<push arg="91"/>
			<load arg="22"/>
			<pcall arg="92"/>
			<dup/>
			<push arg="93"/>
			<getasm/>
			<get arg="94"/>
			<push arg="46"/>
			<call arg="95"/>
			<dup/>
			<store arg="32"/>
			<pcall arg="96"/>
			<dup/>
			<push arg="157"/>
			<getasm/>
			<get arg="94"/>
			<load arg="22"/>
			<call arg="158"/>
			<call arg="95"/>
			<dup/>
			<store arg="114"/>
			<pcall arg="96"/>
			<dup/>
			<push arg="159"/>
			<push arg="160"/>
			<push arg="99"/>
			<findme/>
			<push arg="161"/>
			<call arg="162"/>
			<call arg="71"/>
			<dup/>
			<store arg="116"/>
			<pcall arg="96"/>
			<dup/>
			<push arg="163"/>
			<push arg="31"/>
			<push arg="9"/>
			<new/>
			<push arg="164"/>
			<push arg="99"/>
			<findme/>
			<call arg="165"/>
			<iterate/>
			<store arg="118"/>
			<load arg="118"/>
			<get arg="41"/>
			<push arg="166"/>
			<call arg="56"/>
			<call arg="68"/>
			<if arg="167"/>
			<load arg="118"/>
			<call arg="70"/>
			<enditerate/>
			<call arg="71"/>
			<dup/>
			<store arg="118"/>
			<pcall arg="96"/>
			<dup/>
			<push arg="97"/>
			<push arg="98"/>
			<push arg="99"/>
			<new/>
			<pcall arg="100"/>
			<pusht/>
			<pcall arg="101"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="168" begin="7" end="7"/>
			<lne id="169" begin="7" end="8"/>
			<lne id="170" begin="25" end="25"/>
			<lne id="171" begin="25" end="26"/>
			<lne id="172" begin="27" end="27"/>
			<lne id="173" begin="25" end="28"/>
			<lne id="174" begin="34" end="34"/>
			<lne id="175" begin="34" end="35"/>
			<lne id="176" begin="36" end="36"/>
			<lne id="177" begin="36" end="37"/>
			<lne id="178" begin="34" end="38"/>
			<lne id="179" begin="44" end="46"/>
			<lne id="180" begin="47" end="47"/>
			<lne id="181" begin="44" end="48"/>
			<lne id="182" begin="44" end="49"/>
			<lne id="183" begin="58" end="60"/>
			<lne id="184" begin="58" end="61"/>
			<lne id="185" begin="64" end="64"/>
			<lne id="186" begin="64" end="65"/>
			<lne id="187" begin="66" end="66"/>
			<lne id="188" begin="64" end="67"/>
			<lne id="189" begin="55" end="72"/>
			<lne id="190" begin="55" end="73"/>
			<lne id="191" begin="77" end="82"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="153" begin="63" end="71"/>
			<lve slot="2" name="93" begin="30" end="82"/>
			<lve slot="3" name="157" begin="40" end="82"/>
			<lve slot="4" name="159" begin="51" end="82"/>
			<lve slot="5" name="163" begin="75" end="82"/>
			<lve slot="1" name="91" begin="6" end="84"/>
			<lve slot="0" name="20" begin="0" end="85"/>
		</localvariabletable>
	</operation>
	<operation name="192">
		<context type="7"/>
		<parameters>
			<parameter name="22" type="111"/>
		</parameters>
		<code>
			<load arg="22"/>
			<push arg="91"/>
			<call arg="112"/>
			<store arg="32"/>
			<load arg="22"/>
			<push arg="97"/>
			<call arg="113"/>
			<store arg="114"/>
			<load arg="22"/>
			<push arg="93"/>
			<call arg="115"/>
			<store arg="116"/>
			<load arg="22"/>
			<push arg="157"/>
			<call arg="115"/>
			<store arg="118"/>
			<load arg="22"/>
			<push arg="159"/>
			<call arg="115"/>
			<store arg="193"/>
			<load arg="22"/>
			<push arg="163"/>
			<call arg="115"/>
			<store arg="194"/>
			<load arg="114"/>
			<dup/>
			<getasm/>
			<load arg="32"/>
			<get arg="41"/>
			<call arg="33"/>
			<set arg="41"/>
			<dup/>
			<getasm/>
			<push arg="31"/>
			<push arg="9"/>
			<new/>
			<load arg="32"/>
			<get arg="117"/>
			<iterate/>
			<store arg="195"/>
			<getasm/>
			<load arg="195"/>
			<push arg="119"/>
			<call arg="120"/>
			<call arg="70"/>
			<enditerate/>
			<call arg="33"/>
			<set arg="121"/>
			<pop/>
			<load arg="193"/>
			<load arg="194"/>
			<pcall arg="196"/>
			<load arg="114"/>
			<load arg="116"/>
			<pcall arg="122"/>
			<load arg="114"/>
			<load arg="116"/>
			<push arg="123"/>
			<load arg="32"/>
			<get arg="123"/>
			<pcall arg="124"/>
			<load arg="114"/>
			<load arg="116"/>
			<push arg="125"/>
			<load arg="32"/>
			<get arg="125"/>
			<pcall arg="124"/>
			<load arg="114"/>
			<load arg="118"/>
			<pcall arg="122"/>
		</code>
		<linenumbertable>
			<lne id="197" begin="27" end="27"/>
			<lne id="198" begin="27" end="28"/>
			<lne id="199" begin="25" end="30"/>
			<lne id="200" begin="36" end="36"/>
			<lne id="201" begin="36" end="37"/>
			<lne id="202" begin="40" end="40"/>
			<lne id="203" begin="41" end="41"/>
			<lne id="204" begin="42" end="42"/>
			<lne id="205" begin="40" end="43"/>
			<lne id="206" begin="33" end="45"/>
			<lne id="207" begin="31" end="47"/>
			<lne id="191" begin="24" end="48"/>
			<lne id="208" begin="49" end="49"/>
			<lne id="209" begin="50" end="50"/>
			<lne id="210" begin="49" end="51"/>
			<lne id="211" begin="52" end="52"/>
			<lne id="212" begin="53" end="53"/>
			<lne id="213" begin="52" end="54"/>
			<lne id="214" begin="55" end="55"/>
			<lne id="215" begin="56" end="56"/>
			<lne id="216" begin="57" end="57"/>
			<lne id="217" begin="58" end="58"/>
			<lne id="218" begin="58" end="59"/>
			<lne id="219" begin="55" end="60"/>
			<lne id="220" begin="61" end="61"/>
			<lne id="221" begin="62" end="62"/>
			<lne id="222" begin="63" end="63"/>
			<lne id="223" begin="64" end="64"/>
			<lne id="224" begin="64" end="65"/>
			<lne id="225" begin="61" end="66"/>
			<lne id="226" begin="67" end="67"/>
			<lne id="227" begin="68" end="68"/>
			<lne id="228" begin="67" end="69"/>
			<lne id="229" begin="49" end="69"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="8" name="153" begin="39" end="44"/>
			<lve slot="4" name="93" begin="11" end="69"/>
			<lve slot="5" name="157" begin="15" end="69"/>
			<lve slot="6" name="159" begin="19" end="69"/>
			<lve slot="7" name="163" begin="23" end="69"/>
			<lve slot="3" name="97" begin="7" end="69"/>
			<lve slot="2" name="91" begin="3" end="69"/>
			<lve slot="0" name="20" begin="0" end="69"/>
			<lve slot="1" name="154" begin="0" end="69"/>
		</localvariabletable>
	</operation>
</asm>
