package br.com.cawal.obo2umlconverter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ExecutionConfig {

  private String sourceMetamodel;
  private String targetMetamodel;
  private boolean keepIntermediaryFiles;


  private List<File> inputFiles = new ArrayList<File>();
  private List<File> outputFiles = new ArrayList<File>();
  
  public boolean keepIntermediaryFiles() {
    return keepIntermediaryFiles;
  }

  public void setKeepIntermediaryFiles(boolean keepIntermediaryFiles) {
    this.keepIntermediaryFiles = keepIntermediaryFiles;
  }



  public String getSourceMetamodel() {
    return sourceMetamodel;
  }

  public void setSourceMetamodel(String sourceMetamodel) {
    this.sourceMetamodel = sourceMetamodel;
  }

  public String getTargetMetamodel() {
    return targetMetamodel;
  }

  public void setTargetMetamodel(String targetMetamodel) {
    this.targetMetamodel = targetMetamodel;
  }

  public List<File> getInputFiles() {
    return inputFiles;
  }

  public List<File> getOutputFiles() {
    return outputFiles;
  }
}
